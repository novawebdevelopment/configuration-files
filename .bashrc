# WARNING: This file is automatically retrieved from GitLab.
#          Any changes made locally will be lost.


# Don't do anything if not running interactively
[ -z "$PS1" ] && return

# Check if stdout is a terminal
if test -t 1; then
    ncolors=$(tput colors)

    # Enable colors if supported
    if test -n "$ncolors" && test $ncolors -ge 8; then
        eval "$(dircolors)"
        alias dir='dir --color=auto'
        alias grep='grep --color=auto'
        alias ls='ls --color=auto'
        PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
    else
        PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
    fi

    echo -e "\nYou have successfully logged on to \033[1;33m${HOSTNAME^^}\033[0m."
    echo -e "\033[1;30mThe authorized keys on this server are automatically kept up-to-date.\033[0m"
fi
